public class WildPickUp4Card implements Card{
    
    protected String color;
    
    public WildPickUp4Card(String color){
        this.color = color;
    }

    public boolean canPlay(Card uno){
        return true;
    }

    public String getColor(){
        return "wild4";
    }

    public int getNumber(){
        return 14;
    }
}
