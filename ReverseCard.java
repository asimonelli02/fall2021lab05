public class ReverseCard extends Special {
    
    protected String color;

    public ReverseCard(String color){
        super(color);
    }

    public boolean canPlay(Card uno){
        if(uno instanceof ReverseCard == true){
            return true;
        }
        else if(uno.getColor() == color){
            return true;
        }
        else{
            return false;
        }
    }

    public String getColor(){
        return this.color;
    }

    public int getNumber(){
        return 11;
    }
}
