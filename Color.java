public abstract class Color implements Card {
    protected String color;

    public Color(String color){
        this.color = color;
    }
    public abstract String getColor();
}
