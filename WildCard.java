public class WildCard implements Card {
    protected String color;
    
    public WildCard(String color){
        this.color = color;
    }

    public boolean canPlay(Card uno){
        return true;
    }

    public String getColor(){
        return "wild";
    }

    public int getNumber(){
        return 13;
    }
}
