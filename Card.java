public interface Card {

    public boolean canPlay(Card uno);
    public int getNumber();
    public String getColor();
}