import java.util.ArrayList;

public class Deck{

    ArrayList<Card> deck = new ArrayList<Card>();
        
    public Deck(){
        for(int i = 0; i<4; i++){
            WildCard wild = new WildCard("wild");
            WildPickUp4Card wild4 = new WildPickUp4Card("wild4");
            deck.add(wild);
            deck.add(wild4);
        }

        for(int i = 0; i<10; i++){
            NumberedCards numR = new NumberedCards(i, "red");
            deck.add(numR);
        }

        for(int i = 0; i<10; i++){
            NumberedCards numB = new NumberedCards(i, "blue");
            deck.add(numB);
        }

        for(int i = 0; i<10; i++){
            NumberedCards numY = new NumberedCards(i, "yellow");
            deck.add(numY);
        }

        for(int i = 0; i<10; i++){
            NumberedCards numG = new NumberedCards(i, "green");
            deck.add(numG);
        }

        for(int i = 0; i<2; i++){
            ReverseCard reverseR = new ReverseCard("red");
            SkipCard skipR =new SkipCard("red");
            PickUp2Card pick2R = new PickUp2Card("red");
            deck.add(reverseR);
            deck.add(skipR);
            deck.add(pick2R);
        }

        for(int i = 0; i<2; i++){
            ReverseCard reverseB = new ReverseCard("blue");
            SkipCard skipB =new SkipCard("blue");
            PickUp2Card pick2B = new PickUp2Card("blue");
            deck.add(reverseB);
            deck.add(skipB);
            deck.add(pick2B);
        }

        for(int i = 0; i<2; i++){
            ReverseCard reverseY = new ReverseCard("yellow");
            SkipCard skipY =new SkipCard("yellow");
            PickUp2Card pick2Y = new PickUp2Card("yellow");
            deck.add(reverseY);
            deck.add(skipY);
            deck.add(pick2Y);
        }

        for(int i = 0; i<2; i++){
            ReverseCard reverseG = new ReverseCard("green");
            SkipCard skipG =new SkipCard("green");
            PickUp2Card pick2G = new PickUp2Card("green");
            deck.add(reverseG);
            deck.add(skipG);
            deck.add(pick2G);
        }
        
    }

    public void addToDeck(Card uno){
        deck.add(uno);
    }

    public Card draw(){
        ArrayList<Card> nDeck = new ArrayList<Card>(deck);
        deck.remove(0);
        return nDeck.get(0);
    }
    
}
