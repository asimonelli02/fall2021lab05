public class NumberedCards extends Color {
    protected int num;
    protected String color;
    
    public NumberedCards(int num, String color){
        super(color);
        this.num = num;
    }

    public boolean canPlay(Card uno){
        if(this.num == uno.getNumber()){
            return true;
        }
        else if(this.color == uno.getColor()){
            return true;
        }
        else{
            return false;
        }
    }

    public String getColor(){
        return this.color;
    }

    public int getNumber(){
        return this.num;
    }
    
}