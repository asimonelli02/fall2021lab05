public class SkipCard extends Special{
    
    protected String color;

    public SkipCard(String color){
        super(color);
    }

    public boolean canPlay(Card uno){
        if(uno instanceof SkipCard == true){
            return true;
        }
        else if(uno.getColor() == color){
            return true;
        }
        else{
            return false;
        }
    }

    public String getColor(){
        return this.color;
    }

    public int getNumber(){
        return 10;
    }
}

