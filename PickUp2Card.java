public class PickUp2Card extends Special{

    protected String color;
    
    public PickUp2Card(String color){
        super(color);
    }

    public boolean canPlay(Card uno){
        if(uno instanceof PickUp2Card == true){
            return true;
        }
        else if(uno.getColor() == color){
            return true;
        }
        else{
            return false;
        }
    }

    public String getColor(){
        return this.color;
    }

    public int getNumber(){
        return 12;
    }
}
